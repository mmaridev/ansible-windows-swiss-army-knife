# Ansible-Windows Swiss Army knife

A collection of useful playbooks to manage
windows clients via [Ansible](https://www.ansible.com/).

Connection in the example hosts file is done via ssh
and public key but other connections are possible. Reference:
https://docs.ansible.com/ansible/latest/user_guide/windows_setup.html

Installation of OpenSSH Server can be done via PowerShell with a few
commands:

```
Add-WindowsCapability -Online -Name OpenSSH.Server~~~~0.0.1.0

Set-Service -Name sshd -StartupType 'Automatic'

New-ItemProperty -Path "HKLM:\SOFTWARE\OpenSSH" -Name DefaultShell -Value "C:\Windows\System32\WindowsPowerShell\v1.0\powershell.exe" -PropertyType String -Force

Restart-Service sshd
```

Then copy your ssh key to `\programdata\ssh\administrators_authorized_keys`.
